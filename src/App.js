import './App.css';
import logo from './images/logo.svg'
import plus from './images/plus.svg'
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image'

function App() {
  return (
    <div className="App">
    <Navbar variant='dark' className='Navbar'>
      <Container>
          <Navbar.Brand href="#home">
          <Image src= {logo}
              width="150"
              height="30"
              className="d-inline-block align-top">
          </Image>
          </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />

          <Nav className="me-auto">
            <Nav.Link href="#home">Главная</Nav.Link>
            <Nav.Link href="#afisha">Афиша</Nav.Link>
            <Nav.Link href="#help">Помощь</Nav.Link>
            <Nav.Link href="#about">О нас</Nav.Link>         
          </Nav>
          <Navbar.Collapse className="justify-content-end">
              <Button variant="light"><Image src={plus} width={16} height={16} ></Image>&nbsp;&nbsp;Добавить место</Button>
            </Navbar.Collapse>
      </Container>
    </Navbar>
    </div>
  );
}

export default App;
